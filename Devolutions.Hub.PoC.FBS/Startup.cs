﻿namespace Devolutions.Hub.PoC.FBS
{
    using Devolutions.Hub.PoC.FBS.Business.Repositories;
    using Devolutions.Hub.PoC.FBS.Configuration;
    using Devolutions.Hub.PoC.FBS.Storage;

    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        public IConfiguration Configuration { get; private set; }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.Configure<BlobStorage>(this.Configuration.GetSection("BlobStorage"));

            services.AddSingleton<FileStorageService>();
            services.AddSingleton<LogStorageService>();

            services.AddScoped<HubRepository>();
            services.AddScoped<VaultRepository>();
            services.AddScoped<EntryRepository>();
        }
    }
}