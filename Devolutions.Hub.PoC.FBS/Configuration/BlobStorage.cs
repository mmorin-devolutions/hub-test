﻿namespace Devolutions.Hub.PoC.FBS.Configuration
{
    public class BlobStorage
    {
        public string ConnectionString { get; set; }

        public string EntryHistoryContainerName { get; set; }
    }
}