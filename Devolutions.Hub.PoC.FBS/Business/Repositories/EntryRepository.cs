﻿namespace Devolutions.Hub.PoC.FBS.Business.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using Devolutions.Hub.PoC.FBS.Business.Entities;
    using Devolutions.Hub.PoC.FBS.Storage;

    using Microsoft.AspNetCore.JsonPatch;

    public class EntryRepository
    {
        private readonly FileStorageService fileStorageService;

        private readonly LogStorageService logStorageService;

        public EntryRepository(FileStorageService fileStorageService, LogStorageService logStorageService)
        {
            this.fileStorageService = fileStorageService;
            this.logStorageService = logStorageService;
        }

        public async void Add(EntryContext entryContext, Entry entry)
        {
            StorageContext storageContext = new StorageContext.Builder().ContainerName(entryContext.HubId.ToString()).FileName(this.GetFileName(entry.Id.ToString())).CreateIfNotExists(true).Build();
            await this.fileStorageService.Store(storageContext, entry);

            await this.AppendLog(entryContext, entry, new Log() { Message = "ENTRY_ADDED", Arguments =  new[] { entry.Metadata.Name } });
        }

        public async void Delete(EntryContext entryContext, Guid id)
        {
            var entry = await this.Get(entryContext, id);

            StorageContext storageContext = new StorageContext.Builder().ContainerName(entryContext.HubId.ToString()).FileName(this.GetFileName(id.ToString())).CreateIfNotExists(false).Build();
            await this.fileStorageService.Delete(storageContext);

            await this.AppendLog(entryContext, entry, new Log() { Message = "ENTRY_DELETED", Arguments = new[] { entry.Metadata.Name } });
        }

        public async void Edit(EntryContext entryContext, Entry entry)
        {
            StorageContext storageContext = new StorageContext.Builder().ContainerName(entryContext.HubId.ToString()).FileName(this.GetFileName(entry.Id.ToString())).CreateIfNotExists(false).Build();
            await this.fileStorageService.Store(storageContext, entry);

            await this.AppendLog(entryContext, entry, new Log() { Message = "ENTRY_EDITED", Arguments = new[] { entry.Metadata.Name } });
        }

        public async Task<Entry> Get(EntryContext entryContext, Guid id)
        {
            return await this.LoadEntry(entryContext, id);
        }

        public async Task<List<Log>> GetLogs(EntryContext entryContext, Guid entryId)
        {
            StorageContext storageContext = new StorageContext.Builder().ContainerName(entryContext.HubId.ToString()).FileName(this.GetLogName(entryId.ToString())).CreateIfNotExists(false).Build();
            return await this.logStorageService.Read(storageContext);
        }

        public async void Patch(EntryContext entryContext, Guid id, JsonPatchDocument patchDocument)
        {
            var entry = await this.Get(entryContext, id);
            patchDocument.ApplyTo(entry);

            this.Edit(entryContext, entry);
        }

        private async Task AppendLog(EntryContext entryContext, Entry entry, Log logToAppend)
        {
            StorageContext context = new StorageContext.Builder().ContainerName(entryContext.HubId.ToString()).FileName(this.GetLogName(entry.Id.ToString())).CreateIfNotExists(true).Build();
            await this.logStorageService.Append(context, logToAppend);
        }

        private string GetFileName(string entryId)
        {
            return $"entry.{entryId}.json";
        }

        private string GetLogName(string entryId)
        {
            return $"entry.{entryId}.logs.json";
        }

        private Task<Entry> LoadEntry(EntryContext entryContext, Guid entryId)
        {
            StorageContext context = new StorageContext.Builder().ContainerName(entryContext.HubId.ToString()).FileName(this.GetFileName(entryId.ToString())).CreateIfNotExists(false).Build();
            return this.fileStorageService.Read<Entry>(context);
        }
    }
}