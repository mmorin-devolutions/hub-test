﻿namespace Devolutions.Hub.PoC.FBS.Business.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using Devolutions.Hub.PoC.FBS.Business.Entities;
    using Devolutions.Hub.PoC.FBS.Storage;

    using Microsoft.AspNetCore.JsonPatch;

    public class HubRepository
    {
        private const string STORAGE_FILE_NAME = "hub.json";

        private const string STORAGE_LOG_NAME = "hub.log.json";

        private readonly FileStorageService fileStorageService;

        private readonly LogStorageService logStorageService;

        public HubRepository(FileStorageService fileStorageService, LogStorageService logStorageService)
        {
            this.fileStorageService = fileStorageService;
            this.logStorageService = logStorageService;
        }

        public async Task Add(Hub hub)
        {
            await this.Save(hub);
            await this.AppendLog(hub, new Log() { Message = "HUB_CREATED" });
        }

        public async Task Edit(Hub hub)
        {
            await this.Save(hub);
            await this.AppendLog(hub, new Log() { Message = "HUB_EDITED" });
        }

        public async Task<Hub> Get(Guid id)
        {
            return await this.Load(id);
        }
        
        public async Task<List<Log>> GetLogs(Guid id)
        {
            return await this.logStorageService.Read(this.GetLogStorageContext(id));
        }

        public async void Patch(Guid id, JsonPatchDocument patchDocument)
        {
            var hub = await this.Load(id);
            patchDocument.ApplyTo(hub);
            await this.Edit(hub);
        }

        private async Task AppendLog(Hub hub, Log logToAppend)
        {
            await this.logStorageService.Append(this.GetLogStorageContext(hub.Id), logToAppend);
        }

        private async Task<Hub> Load(Guid id)
        {
            return await this.fileStorageService.Read<Hub>(this.GetFileStorageContext(id));
        }

        private async Task Save(Hub hub)
        {
            await this.fileStorageService.Store(this.GetFileStorageContext(hub.Id), hub);
        }

        private StorageContext GetFileStorageContext(Guid id)
        {
            return new StorageContext.Builder().ContainerName(id.ToString()).FileName(STORAGE_FILE_NAME).CreateIfNotExists(true).Build();
        }

        private StorageContext GetLogStorageContext(Guid id)
        {
            return new StorageContext.Builder().ContainerName(id.ToString()).FileName(STORAGE_LOG_NAME).CreateIfNotExists(true).Build();
        }
    }
}