﻿namespace Devolutions.Hub.PoC.FBS.Business.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using Devolutions.Hub.PoC.FBS.Business.Entities;
    using Devolutions.Hub.PoC.FBS.Storage;

    using Microsoft.AspNetCore.JsonPatch;

    public class VaultRepository
    {
        private readonly FileStorageService fileStorageService;

        private readonly LogStorageService logStorageService;

        public VaultRepository(FileStorageService fileStorageService, LogStorageService logStorageService)
        {
            this.fileStorageService = fileStorageService;
            this.logStorageService = logStorageService;
        }

        public async void Add(Guid hubId, Vault vault)
        {
            StorageContext context = new StorageContext.Builder().ContainerName(hubId.ToString()).FileName(this.GetFileName(vault.Id.ToString())).CreateIfNotExists(true).Build();
            await this.fileStorageService.Store(context, vault);

            await this.AppendLog(hubId, vault, new Log() { Message = "VAULT_ADDED", Arguments = new[] { vault.Name } });
        }

        public async void Delete(Guid hubId, Guid vaultId)
        {
            var vault = await this.Get(hubId, vaultId);

            StorageContext context = new StorageContext.Builder().ContainerName(hubId.ToString()).FileName(this.GetFileName(vaultId.ToString())).CreateIfNotExists(false).Build();
            await this.fileStorageService.Delete(context);

            await this.AppendLog(hubId, vault, new Log() { Message = "VAULT_DELETED", Arguments = new[] { vault.Name } });
        }

        public async Task Edit(Guid hubId, Vault vault)
        {
            StorageContext context = new StorageContext.Builder().ContainerName(hubId.ToString()).FileName(this.GetFileName(vault.Id.ToString())).CreateIfNotExists(false).Build();
            await this.fileStorageService.Store(context, vault);

            await this.AppendLog(hubId, vault, new Log() { Message = "VAULT_EDITED", Arguments = new[] { vault.Name } });
        }

        public async Task<Vault> Get(Guid hubId, Guid vaultId)
        {
            return await this.LoadVault(hubId, vaultId);
        }

        public async Task<List<Log>> GetLogs(Guid hubId, Guid vaultId)
        {
            StorageContext context = new StorageContext.Builder().ContainerName(hubId.ToString()).FileName(this.GetLogName(vaultId.ToString())).CreateIfNotExists(false).Build();
            return await this.logStorageService.Read(context);
        }

        public async void Patch(Guid hubId, Guid vaultId, JsonPatchDocument patchDocument)
        {
            var vault = await this.Get(hubId, vaultId);
            patchDocument.ApplyTo(vault);

            await this.Edit(hubId, vault);
        }

        private async Task AppendLog(Guid hubId, Vault vault, Log logToAppend)
        {
            StorageContext context = new StorageContext.Builder().ContainerName(hubId.ToString()).FileName(this.GetLogName(vault.Id.ToString())).CreateIfNotExists(true).Build();
            await this.logStorageService.Append(context, logToAppend);
        }

        private string GetFileName(string vaultId)
        {
            return $"vault.{vaultId}.json";
        }

        private string GetLogName(string vaultId)
        {
            return $"vault.{vaultId}.logs.json";
        }

        private Task<Vault> LoadVault(Guid hubId, Guid vaultId)
        {
            StorageContext context = new StorageContext.Builder().ContainerName(hubId.ToString()).FileName(this.GetFileName(vaultId.ToString())).CreateIfNotExists(false).Build();
            return this.fileStorageService.Read<Vault>(context);
        }
    }
}