﻿using System;
using System.Collections.Generic;

namespace Devolutions.Hub.PoC.FBS.Business.Entities
{
    public class Hub
    {
        public Hub()
        {
            this.Vaults = new List<Guid>();
        }

        public Guid Id { get; set; }

        public List<Guid> Vaults { get; set; }
    }
}