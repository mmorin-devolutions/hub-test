﻿namespace Devolutions.Hub.PoC.FBS.Business.Entities
{
    using System;

    public class Log
    {
        public Log()
        {
            this.Arguments = new string[0];
            this.DateTime = DateTime.Now;
        }

        public string[] Arguments { get; set; }

        public DateTime DateTime { get; set; }

        public string Message { get; set; }

    }
}