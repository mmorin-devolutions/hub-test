﻿namespace Devolutions.Hub.PoC.FBS.Business.Entities
{
    using System;

    public class Entry
    {
        public Entry()
        {
            this.Metadata = new EntryMetadata();
        }

        public string Data { get; set; }

        public Guid Id { get; set; }

        public EntryMetadata Metadata { get; set; }
    }

    public class EntryMetadata
    {
        public string Description { get; set; }

        public string Name { get; set; }
    }

    public class EntryContext
    {
        public EntryContext(Guid hubId, Guid vaultId)
        {
            this.HubId = hubId;
            this.VaultId = vaultId;
        }

        public Guid HubId { get; set; }

        public Guid VaultId { get; set; }
    }
}