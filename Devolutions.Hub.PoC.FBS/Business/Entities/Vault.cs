﻿namespace Devolutions.Hub.PoC.FBS.Business.Entities
{
    using System;
    using System.Collections.Generic;

    public class Vault
    {
        public Vault()
        {
            this.Entries = new List<Guid>();
        }

        public List<Guid> Entries { get; set; }

        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}