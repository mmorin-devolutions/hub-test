﻿namespace Devolutions.Hub.PoC.FBS.Storage
{
    public class StorageContext
    {
        private StorageContext()
        {
        }

        public string ContainerName { get; private set; }

        public bool CreateIfNotExists { get; private set; }

        public string FileName { get; private set; }

        public class Builder
        {
            private readonly StorageContext context;

            public Builder()
            {
                this.context = new StorageContext();
                this.context.CreateIfNotExists = true;
            }

            public StorageContext Build()
            {
                return this.context;
            }

            public Builder ContainerName(string containerName)
            {
                this.context.ContainerName = containerName;
                return this;
            }

            public Builder CreateIfNotExists(bool create)
            {
                this.context.CreateIfNotExists = create;
                return this;
            }

            public Builder FileName(string fileName)
            {
                this.context.FileName = fileName;
                return this;
            }
        }
    }
}