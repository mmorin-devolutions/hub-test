﻿namespace Devolutions.Hub.PoC.FBS.Storage
{
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;

    using Devolutions.Hub.PoC.FBS.Business.Entities;
    using Devolutions.Hub.PoC.FBS.Configuration;

    using Microsoft.Azure.Storage;
    using Microsoft.Azure.Storage.Blob;
    using Microsoft.Extensions.Options;

    using Newtonsoft.Json;

    public class LogStorageService
    {
        private readonly CloudBlobClient blobClient;

        public LogStorageService(IOptions<BlobStorage> config)
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(config.Value.ConnectionString);
            this.blobClient = storageAccount.CreateCloudBlobClient();
        }

        public async Task Append(StorageContext context, Log toAppend)
        {
            var logAsString = JsonConvert.SerializeObject(toAppend);
            logAsString += ",";

            CloudAppendBlob appendBlob = await this.GetAppendBlob(context);
            byte[] byteArray = Encoding.UTF8.GetBytes(logAsString);
            using (MemoryStream stream = new MemoryStream(byteArray))
            {
                await appendBlob.AppendBlockAsync(stream);
            }
        }

        public async Task<List<Log>> Read(StorageContext context)
        {
            CloudAppendBlob appendBlob = await this.GetAppendBlob(context);

            var accessCondition = AccessCondition.GenerateEmptyCondition();
            var blobRequestOptions = new BlobRequestOptions();
            var operationContext = new OperationContext();

            var data = await appendBlob.DownloadTextAsync(Encoding.UTF8, accessCondition, blobRequestOptions, operationContext);
            var dataAsJsonArray = $"[{data.Substring(0, data.Length - 1)}]";

            return JsonConvert.DeserializeObject<List<Log>>(dataAsJsonArray);
        }

        private async Task<CloudAppendBlob> GetAppendBlob(StorageContext context)
        {
            var container = this.blobClient.GetContainerReference(context.ContainerName);
            await container.CreateIfNotExistsAsync();
            var appendBlob = container.GetAppendBlobReference(context.FileName);

            var blobRequestOptions = new BlobRequestOptions();
            var operationContext = new OperationContext();

            try
            {
                await appendBlob.CreateOrReplaceAsync(AccessCondition.GenerateIfNotExistsCondition(), blobRequestOptions, operationContext);
            }
            catch (StorageException se)
            {
                if (se.RequestInformation.HttpStatusCode == (int)HttpStatusCode.Conflict)
                {
                    return appendBlob;
                }

                throw;
            }

            return appendBlob;
        }
    }
}