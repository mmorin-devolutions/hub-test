﻿namespace Devolutions.Hub.PoC.FBS.Storage
{
    using System;
    using System.Text;
    using System.Threading.Tasks;

    using Devolutions.Hub.PoC.FBS.Configuration;

    using Microsoft.Azure.Storage;
    using Microsoft.Azure.Storage.Blob;
    using Microsoft.Azure.Storage.RetryPolicies;
    using Microsoft.Extensions.Options;

    using Newtonsoft.Json;

    public class FileStorageService
    {
        private const int LEASE_MAX_RETRY = 5;

        private const int LEASE_RETRY_DELAY_IN_MS = 200;

        private const int LEASE_TIME_IN_S = 15;

        private readonly CloudBlobClient blobClient;

        public FileStorageService(IOptions<BlobStorage> config)
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(config.Value.ConnectionString);
            this.blobClient = storageAccount.CreateCloudBlobClient();
        }

        public async Task Delete(StorageContext context)
        {
            CloudBlockBlob blockToStore = await this.GetBlockBlob(context);
            await blockToStore.DeleteAsync();
        }

        public async Task<T> Read<T>(StorageContext context)
        {
            CloudBlockBlob blockToRead = await this.GetBlockBlob(context);

            var accessCondition = AccessCondition.GenerateEmptyCondition();
            var blobRequestOptions = new BlobRequestOptions();
            var operationContext = new OperationContext();

            string data = await blockToRead.DownloadTextAsync(Encoding.UTF8, accessCondition, blobRequestOptions, operationContext);
            return JsonConvert.DeserializeObject<T>(data);
        }

        public async Task Store<T>(StorageContext context, T data)
        {
            CloudBlockBlob blockToStore = await this.GetBlockBlob(context);

            var exists = await blockToStore.ExistsAsync();
            if (!exists && context.CreateIfNotExists)
            {
                await this.StoreAndCreateBlob(blockToStore, data);
            }
            else
            {
                await this.StoreExistingBlob(blockToStore, data);
            }
        }

        private async Task StoreExistingBlob<T>(CloudBlockBlob blockBlob, T data)
        {
            var accessCondition = AccessCondition.GenerateEmptyCondition();
            var blobRequestOptions = new BlobRequestOptions();
            var operationContext = new OperationContext();

            var dataAsString = JsonConvert.SerializeObject(data);
            blobRequestOptions.RetryPolicy = new LinearRetry(TimeSpan.FromMilliseconds(LEASE_RETRY_DELAY_IN_MS), LEASE_MAX_RETRY);
            var lease = await blockBlob.AcquireLeaseAsync(TimeSpan.FromSeconds(LEASE_TIME_IN_S), proposedLeaseId: null, accessCondition, blobRequestOptions, operationContext);

            accessCondition = AccessCondition.GenerateLeaseCondition(lease);
            blobRequestOptions = new BlobRequestOptions();
            await blockBlob.UploadTextAsync(dataAsString, Encoding.UTF8, accessCondition, blobRequestOptions, operationContext);
            await blockBlob.ReleaseLeaseAsync(accessCondition);
        }

        private async Task StoreAndCreateBlob<T>(CloudBlockBlob blockBlob, T data)
        {
            var accessCondition = AccessCondition.GenerateIfNotExistsCondition();
            var blobRequestOptions = new BlobRequestOptions();
            var operationContext = new OperationContext();

            var dataAsString = JsonConvert.SerializeObject(data);

            await blockBlob.UploadTextAsync(dataAsString, Encoding.UTF8, accessCondition, blobRequestOptions, operationContext);
        }

        private async Task<CloudBlockBlob> GetBlockBlob(StorageContext context)
        {
            var container = this.blobClient.GetContainerReference(context.ContainerName);

            if (context.CreateIfNotExists)
            {
                await container.CreateIfNotExistsAsync();
            }

            return container.GetBlockBlobReference(context.FileName);
        }
    }
}