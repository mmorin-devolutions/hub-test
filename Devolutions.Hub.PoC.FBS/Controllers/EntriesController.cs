﻿namespace Devolutions.Hub.PoC.FBS.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Devolutions.Hub.PoC.FBS.Business.Entities;
    using Devolutions.Hub.PoC.FBS.Business.Repositories;

    using Microsoft.AspNetCore.JsonPatch;
    using Microsoft.AspNetCore.Mvc;

    [Route("api/hubs/{hubId}/vaults/{vaultId}/[controller]")]
    [ApiController]
    public class EntriesController : ControllerBase
    {
        private readonly EntryRepository entryRepository;

        private readonly VaultRepository vaultRepository;

        public EntriesController(VaultRepository vaultRepository, EntryRepository entryRepository)
        {
            this.vaultRepository = vaultRepository;
            this.entryRepository = entryRepository;
        }

        [HttpDelete("{entryId}")]
        public async void Delete(Guid hubId, Guid vaultId, Guid entryId)
        {
            var vault = await this.vaultRepository.Get(hubId, vaultId);
            vault.Entries.Remove(entryId);
            await this.vaultRepository.Edit(hubId, vault);

            this.entryRepository.Delete(new EntryContext(hubId, vaultId), entryId);
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Entry>>> Get(Guid hubId, Guid vaultId)
        {
            var vault = await this.vaultRepository.Get(hubId, vaultId);

            var entries = new List<Entry>();
            foreach (var entryId in vault.Entries)
            {
                var entry = await this.entryRepository.Get(new EntryContext(hubId, vaultId), entryId);
                entries.Add(entry);
            }

            return entries;
        }

        [HttpGet("{entryId}")]
        public async Task<ActionResult<Entry>> Get(Guid hubId, Guid vaultId, Guid entryId)
        {
            EntryContext entryContext = new EntryContext(hubId, vaultId);
            return await this.entryRepository.Get(entryContext, entryId);
        }

        [HttpGet("{entryId}/logs")]
        public async Task<ActionResult<List<Log>>> GetLogs(Guid hubId, Guid vaultId, Guid entryId)
        {
            return await this.entryRepository.GetLogs(new EntryContext(hubId, vaultId), entryId);
        }

        [HttpPatch("{entryId}")]
        public ActionResult Patch(Guid hubId, Guid vaultId, Guid entryId, [FromBody] JsonPatchDocument jsonPatchDocument)
        {
            this.entryRepository.Patch(new EntryContext(hubId, vaultId), entryId, jsonPatchDocument);
            return this.Ok();
        }

        [HttpPost]
        public async Task<Entry> Post(Guid hubId, Guid vaultId, [FromBody] Entry entry)
        {
            if (Guid.Empty.Equals(entry.Id))
            {
                entry.Id = Guid.NewGuid();
            }

            this.entryRepository.Add(new EntryContext(hubId, vaultId), entry);
            var vault = await this.vaultRepository.Get(hubId, vaultId);
            vault.Entries.Add(entry.Id);
            await this.vaultRepository.Edit(hubId, vault);

            return entry;
        }

        [HttpPut("{entryId}")]
        public ActionResult Put(Guid hubId, Guid vaultId, Guid entryId, [FromBody] Entry entry)
        {
            if (!entryId.Equals(entry.Id))
            {
                return this.Conflict();
            }

            this.entryRepository.Edit(new EntryContext(hubId, vaultId), entry);
            return this.Ok();
        }
    }
}