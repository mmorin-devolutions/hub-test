﻿namespace Devolutions.Hub.PoC.FBS.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using Devolutions.Hub.PoC.FBS.Business.Entities;
    using Devolutions.Hub.PoC.FBS.Business.Repositories;

    using Microsoft.AspNetCore.JsonPatch;
    using Microsoft.AspNetCore.Mvc;

    [Route("api/[controller]")]
    [ApiController]
    public class HubsController : ControllerBase
    {
        private readonly HubRepository hubRepository;

        public HubsController(HubRepository hubRepository)
        {
            this.hubRepository = hubRepository;
        }

        [HttpGet("{id}")]
        public Task<Hub> Get(Guid id)
        {
            return this.hubRepository.Get(id);
        }
        
        [HttpGet("{id}/logs")]
        public async Task<List<Log>> GetLogs(Guid id)
        {
            return await this.hubRepository.GetLogs(id);
        }

        [HttpPatch("{id}")]
        public ActionResult Patch(Guid id, [FromBody] JsonPatchDocument jsonPatchDocument)
        {
            this.hubRepository.Patch(id, jsonPatchDocument);
            return this.Ok();
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Put(Guid id, [FromBody] Hub hub)
        {
            if (!id.Equals(hub.Id))
            {
                return this.Conflict();
            }

            await this.hubRepository.Edit(hub);
            return this.Ok();
        }

        [HttpPost]
        public async Task Post([FromBody] Hub hub)
        {
            if (Guid.Empty.Equals(hub.Id))
            {
                hub.Id = Guid.NewGuid();
            }

            await this.hubRepository.Add(hub);
        }
    }
}