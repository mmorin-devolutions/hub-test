﻿namespace Devolutions.Hub.PoC.FBS.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Devolutions.Hub.PoC.FBS.Business.Entities;
    using Devolutions.Hub.PoC.FBS.Business.Repositories;

    using Microsoft.AspNetCore.JsonPatch;
    using Microsoft.AspNetCore.Mvc;

    [Route("api/hubs/{hubId}/[controller]")]
    [ApiController]
    public class VaultsController : ControllerBase
    {
        private readonly VaultRepository vaultRepository;
        private readonly HubRepository hubRepository;
        private readonly EntryRepository entryRepository;

        public VaultsController(VaultRepository vaultRepository, HubRepository hubRepository, EntryRepository entryRepository)
        {
            this.vaultRepository = vaultRepository;
            this.hubRepository = hubRepository;
            this.entryRepository = entryRepository;
        }

        [HttpDelete("{vaultId}")]
        public async void Delete(Guid hubId, Guid vaultId)
        {
            var hub = await this.hubRepository.Get(hubId);
            hub.Vaults.Remove(vaultId);
            await this.hubRepository.Edit(hub);

            var vault = await this.vaultRepository.Get(hubId, vaultId);
            this.vaultRepository.Delete(hubId, vaultId);

            vault.Entries.ForEach(entryId => this.entryRepository.Delete(new EntryContext(hubId, vaultId), entryId));
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Vault>>> Get(Guid hubId)
        {
            var hub = await this.hubRepository.Get(hubId);

            var vaults = new List<Vault>();
            foreach (var vaultId in hub.Vaults)
            {
                var vault = await this.vaultRepository.Get(hubId, vaultId);
                vaults.Add(vault);
            }

            return this.Ok(vaults);
        }

        [HttpGet("{vaultId}")]
        public async Task<ActionResult<Vault>> Get(Guid hubId, Guid vaultId)
        {
            return await this.vaultRepository.Get(hubId, vaultId);
        }

        [HttpGet("{vaultId}/logs")]
        public async Task<ActionResult<List<Log>>> GetLogs(Guid hubId, Guid vaultId)
        {
            return await this.vaultRepository.GetLogs(hubId, vaultId);
        }

        [HttpPatch("{vaultId}")]
        public ActionResult Patch(Guid hubId, Guid vaultId, [FromBody] JsonPatchDocument jsonPatchDocument)
        {
            this.vaultRepository.Patch(hubId, vaultId, jsonPatchDocument);
            return this.Ok();
        }

        [HttpPost]
        public async void Post(Guid hubId, [FromBody] Vault vault)
        {
            if (Guid.Empty.Equals(vault.Id))
            {
                vault.Id = Guid.NewGuid();
            }

            var hub = await this.hubRepository.Get(hubId);
            hub.Vaults.Add(vault.Id);
            await this.hubRepository.Edit(hub);

            this.vaultRepository.Add(hubId, vault);
        }

        [HttpPut("{vaultId}")]
        public async Task<ActionResult> Put(Guid hubId, Guid vaultId, [FromBody] Vault vault)
        {
            if (!vaultId.Equals(vault.Id))
            {
                return this.Conflict();
            }

            await this.vaultRepository.Edit(hubId, vault);
            return this.Ok();
        }
    }
}