hub.json -> {}
	- vaults
	- settings
	- password templates
	- users
	- groups
	
	
hub.logs.json -> []
	- message key
	- datetime
	- arguments -> []	
	
	
vault.<id>.json -> {}
	- entries
	- roles
		- permissions
		- users
		- groups


vault.<id>.logs.json -> []
	- message key
	- datetime
	- arguments -> []
	

entry.<id>.json -> {}
	- metadata
		- name
		- description
		- image
		- created on
		- created by
		...
	- data -> Encrypted string
	- attachments
		- name
		- datetime
		...
	- documentation
		- content
		...
	
entry.<id>.logs.json -> []
	- message key
	- datetime
	- arguments -> []
	
attachment.<id>.data
